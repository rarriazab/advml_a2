import pickle
from ex_2_3 import Node, load_params, load_sample, print_tree
from ex_2_3_tree_helper import Tree
import numpy as np
import pandas as pd

def s(node, i, hash_s):

    key = node.name
    if len(hash_s[key]) >= i+1:
        return hash_s[key][i], hash_s

    if len(node.descendants) == 0:
        if i == node.sample:
            return 1, hash_s
        else:
            return 0, hash_s

    s_node = 1
    for n in node.descendants:
        key = n.name
        hash_s[key] = []

        prob = 0
        for j in range(0, len(n.cat[0])):
            s_value, hash_s = s(n, j, hash_s)
            hash_s[key] += [s_value]
            prob += s_value*n.cat[i][j]
        hash_s[key] = np.array(hash_s[key])
        s_node *= prob

    return s_node, hash_s

def s_algorithm(root, post_samples):

    hash_s = {}
    prob = 0
    key = root.name
    hash_s[key] = []
    for ind, p in enumerate(root.cat[0]):
        value_s, hash_s = s(root, ind, hash_s)
        key = root.name
        hash_s[key] += [value_s]
        prob += value_s * p
    hash_s[key] = np.array(hash_s[key])

    post_samples = sample_tree_algorithm(root, hash_s, post_samples)
    return prob, post_samples, hash_s

def sample_tree_algorithm(root, hash_s, post_samples):
    prob = root.cat[0]*hash_s[root.name]
    prob = np.round_(prob/np.sum(prob), 3)
    post_samples[int(root.name)] = [take_sample(prob), prob]

    for n in root.descendants:
        post_samples = sample_tree(n, post_samples, hash_s)

    return post_samples

def sample_tree(node, post_samples, hash_s):
    if node.sample == 'Xv':
        sample_anc = post_samples[int(node.ancestor.name)][0]
        prob = node.cat[sample_anc]*hash_s[node.name]
        prob = np.round_(prob / np.sum(prob), 3)
        post_samples[int(node.name)] = [take_sample(prob), prob]
        for n in node.descendants:
            post_samples = sample_tree(n, post_samples, hash_s)
    else:
        post_samples[int(node.name)] = [node.sample, np.array([1, 0])]

    return post_samples

def take_sample(dist):

    r = np.random.random()
    dist_norm = dist / np.sum(dist)
    for i in range(0, len(dist_norm)):
        if r <= dist_norm[i]:
            return i
        r += dist_norm[i]

    return 0

my_data_path = '../2_5/'

with open(my_data_path+'tree_with_leaf_samples.pkl', 'rb') as handle:
    samples = pickle.load(handle)

with open(my_data_path+'tree_with_CPD.pkl', 'rb') as handle:
    params = pickle.load(handle)

t = Tree()

t.load_params(params)
load_sample(t.root, samples)

post_samples = {}
prob, post_samples, hash_s = s_algorithm(t.root, post_samples)
df = pd.DataFrame.from_dict(post_samples, orient='index', columns=['sample', 'distribution'])
df = df.sort_index()
df.to_csv('Table_values.csv')
