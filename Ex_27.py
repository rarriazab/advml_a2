import pickle
import numpy as np
import matplotlib.pylab as plt
import random

def load_obj(name):
    with open('./' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def forward(sequence, A, B, pi):
    N = len(A)
    T = len(sequence)
    alpha = np.zeros((T, N))
    c = np.zeros(T)
    for i in range(0, N):
        alpha[0, i] = pi[i] * B[i, sequence[0]]
        c[0] += alpha[0,i]
    if c[0] == 0:
        c[0] = 1e-7
    c[0] = 1.0/c[0]
    for i in range(0, N):
        alpha[0, i] *= c[0]

    for t in np.arange(1, T):
        for i in range(0, N):
            for j in range(0, N):
                alpha[t,i] += alpha[t-1,j]*A[i,j]
            alpha[t,i] = alpha[t,i]*B[i,sequence[t]]
            c[t] += alpha[t,i]
        if c[t] == 0:
            c[t] = 1e-7
        c[t] = 1.0/c[t]
        for i in range(0, N):
            alpha[t, i] *= c[t]

    return alpha, c #+ 1e-6

def backwards(sequence, A, B, c):
    N = len(A)
    T = len(sequence)
    beta = np.zeros((T, N))
    for i in range(0, N):
        beta[T-1, i] = c[T-1]
    for t in reversed(range(T-1)):
        for i in range(0, N):
            for j in range(0, N):
                beta[t, i] += A[i,j]*B[j, sequence[t+1]]*beta[t+1, j]
            beta[t, i] *= c[t]
    return beta

def Compute_gamma(alpha, beta, sequence, A, B):
    N = len(A)
    T = len(sequence)
    gamma = np.zeros((T, N))
    bigamma = np.zeros((T, N, N))
    for t in np.arange(0, T-1):
        denom = 0
        for i in np.arange(0, N):
            for j in np.arange(0, N):
                denom += alpha[t, i] * A[i, j] * B[j, sequence[t+1]] * beta[t+1, j]

        if denom == 0:
            denom = 1e-7
        for i in np.arange(0, N):
            for j in np.arange(0, N):
                bigamma[t, i, j] = (alpha[t, i]*A[i,j]*B[j, sequence[t+1]]*beta[t+1, j])/denom
                gamma[t, i] += bigamma[t, i, j]

    denom = 0
    for i in np.arange(0, N):
        denom += alpha[-1, i]
    if denom == 0:
        denom = 1e-7
    for i in np.arange(0, N):
        gamma[-1, i] = alpha[-1, i]/denom

    return gamma, bigamma #check if we need to sum 1e-6

def A_prior(N, NC):
    Aprior = {}
    for c in range(NC):
        A = np.zeros((N, N))
        for i in range(N):
            for j in range(N):
                A[i,j] = 1.0/N + 0.1*np.random.rand()
            A[i, :] = A[i, :]/np.sum(A[i, :])
        Aprior[c] = A
    return Aprior

def B_prior(N, M, NC):
    Bprior = {}
    gauss = np.array([10.0*np.exp(-abs(N/2-i)) for i in range(N)])
    for c in range(NC):
        B = np.zeros((N, M))
        for i in range(N):
            if N/2 > i:
                B[i, :] = np.hstack([gauss[N/2-i::], gauss[0:N/2-i]])
            else:
                B[i, :] = np.hstack([gauss[N-(i-N/2)::], gauss[0: N-(i-N/2)]])
            B[i, :] = B[i, :]/np.sum(B[i, :])
        Bprior[c] = B
    return Bprior

def pi_prior(N, NC):
    piprior = {}
    for c in range(NC):
        pi = np.zeros(N)
        for i in range(N):
            pi[i] = 1.0/N + np.random.rand()
        pi = pi/np.sum(pi)
        piprior[c] = pi
    return piprior

def init_z(NC):
    z = np.zeros(NC)
    for i in range(NC):
        z[i] = 1.0/NC
    return z

def prior_class(N, NC, data, Aprior, Bprior, piprior):
    classes = np.zeros(len(data))
    temp_lh = np.zeros((len(data), NC))
    for i, d in enumerate(data):
        for cl in range(NC):
            alpha, c = forward(d, Aprior[cl], Bprior[cl], piprior[cl])
            temp_lh[i, cl] = -np.sum(np.log(c))
        ind_max = np.where(temp_lh[i, :] == np.max(temp_lh[i, :]))[0]
        if len(ind_max) >= 1:
            if len(ind_max) > 1:
                classes[i] = ind_max[0]
            else:
                classes[i] = ind_max
        else:
            classes[i] = random.randint(0, len(z) - 1)
        classes = classes.astype(int)

    return classes

def update_z(est_class, NC, N):# (est_class, PO, NC):
    z = np.zeros(NC)
    for i in range(NC):
        num_i = len(np.where(est_class == i)[0])
        z[i] = num_i
    z = z/len(est_class)
    return z

def re_estimate_HMM(observations, gammas, digammas, len_cluster, N, old_params):
    try:
        len_T = len(observations[0])

        #compute A
        A = np.zeros((N,N))
        for m in range(N):
            den = 0.0
            for k in range(len_cluster):
                for t in range(len_T):
                    den += gammas[k][t][m]

            if den == 0.0:
                den = 1e-7
            for n in range(N):
                num = 0.0
                for k in range(len_cluster):
                    for t in range(len_T):
                        num += digammas[k][t][m, n]
                A[m, n] = num/den
            if np.sum(A[m, :]) == 0:
                A[m, :] = np.zeros(N)
            else:
                A[m, :] = A[m, :]/np.sum(A[m, :])

        #compute B
        B = np.zeros((N, N))
        for m in range(N):
            for n in range(N):
                num = 0.0
                den = 0.0
                for k in range(len_cluster):
                    for t in range(len_T):
                        if m == observations[k][t]:
                            num += gammas[k][t][n]
                        den += gammas[k][t][n]
                if den == 0.0:
                    den = 1e-7
                B[m,n] = num/den
            if np.sum(B[m, :]) == 0:
                B[m, :] = np.zeros(N)
            else:
                B[m, :] = B[m, :] / np.sum(B[m, :])

        #compute pi
        pi = np.zeros(N)
        for n in range(N):
            num = 0.0
            for k in range(len_cluster):
                num += gammas[k][0][n]
            pi[n] = num/len_cluster
    except:
        A, B, pi = old_params['A'], old_params['B'], old_params['pi']
    return A, B, pi

def training(classes, NC, N, A, B, pi, data):
    Train_likelihood = np.zeros(len(data))
    for c in range(NC):
        alphas = {}
        betas = {}
        gammas = {}
        digammas = {}
        obs_class = {}
        index_class = np.where(classes == c)[0]
        likelihoods = np.zeros(len(index_class))
        for i, ind in enumerate(index_class):
            alphas[i], c_like = forward(data[ind], A[c], B[c], pi[c])
            betas[i] = backwards(data[ind], A[c], B[c], c_like)
            likelihoods[i] = -np.sum(np.log(c_like)) #(np.log(c_like)).prod()#
            Train_likelihood[ind] = likelihoods[i]
            gammas[i], digammas[i] = Compute_gamma(alphas[i], betas[i], data[ind], A[c], B[c])
            obs_class[i] = data[ind]
        old_params = {}
        old_params['A'] = A[c]
        old_params['B'] = B[c]
        old_params['pi'] = pi[c]
        A[c], B[c], pi[c] = re_estimate_HMM(obs_class, gammas, digammas, len(index_class), N, old_params)

    return A, B, pi, Train_likelihood.sum()/len(data)

targets = load_obj("targets")
data = load_obj("data")
class_prob = load_obj("class_prob")
start_prob = load_obj("start_prob")
transition_prob = load_obj("transition_prob")
emission_prob = load_obj("emission_prob")

N = transition_prob[0].shape[0]
M = emission_prob[0].shape[1]
NC = len(transition_prob)
Aprior = A_prior(N, NC)
Bprior = B_prior(N, M, NC)
piprior = pi_prior(N, NC) # probability of starting at each row

est_class = prior_class(N, NC, data, Aprior, Bprior, piprior) # estimation of classes
A, B, pi, new_log = training(est_class, NC, N, Aprior, Bprior, piprior, data)

z = np.ones(NC)*(1.0/NC)
p_class = {}
old_log = -np.inf
iter = 0
plot_like = []

prev_A = {}
prev_B = {}
prev_pi = {}
prev_est = est_class
prev_z = z

while( (new_log > old_log)  and (np.mean(abs(z-class_prob)))>0.05 ):
    #compute the most likely HMM to each data
    # E step
    if iter != 0:
        old_log = new_log
    temp_lh = np.zeros((len(data), NC))
    PO = np.zeros((len(data), NC))
    for i, d in enumerate(data):
        comp_lh = 0
        for cl in range(NC):
            alpha, c = forward(d, A[cl], B[cl], pi[cl])
            beta = backwards(d, A[cl], B[cl], c)
            temp_lh[i, cl] = -np.sum(np.log(c)) #(np.log(c)).prod()#
        ind_max = np.where(temp_lh[i, :] == np.max(temp_lh[i, :]))[0]
        if len(ind_max) >= 1:
            if len(ind_max) > 1:
                est_class[i] = ind_max[0]
            else:
                est_class[i] = ind_max
        else:
            est_class[i] = random.randint(0, len(z)-1)
    est_class = est_class.astype(int)

    # M Step
    A, B, pi, new_log = training(est_class, NC, N, A, B, pi, data)

    z = update_z(est_class, NC, N) #(est_class, PO, NC) #

    prev_A[0], prev_A[1], prev_A[2] = A[0], A[1], A[2]
    prev_B[0], prev_B[1], prev_B[2] = B[0], B[1], B[2]
    prev_pi[0], prev_pi[1], prev_pi[2] = pi[0], pi[1], pi[2]
    prev_est = est_class
    prev_z = z
    #printing
    plot_like += [new_log]
    print('Iteration: {}'.format(iter))
    print('Classes: {}'.format(est_class))
    print('Rclasse: {}'.format(targets))
    print('Likelihood: {}'.format(new_log))
    print('Difference: {}'.format(abs(old_log - new_log)))
    print('Difference z: {}'.format(np.mean(abs(z-class_prob))))
    print('\n')
    iter += 1

print('Final:')
print('z: {}'.format(prev_z))
print('t: {}'.format(class_prob))
print('targets: {}'.format(targets))
print('realest: {}'.format(prev_est))
plt.plot(range(len(plot_like)-1), plot_like[0:-1])
plt.xlabel('Iteration', fontsize=18)
plt.ylabel('Log-Likelihood', fontsize=18)
plt.savefig('2_7_fig_otro.eps')
plt.show()
