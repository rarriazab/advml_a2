import pickle
from ex_2_3 import Node, load_params, load_sample, print_tree
from ex_2_3_tree_helper import Tree
import numpy as np
from Ex_2_3_10 import s, s_algorithm

my_data_path = ''

with open(my_data_path + 'tree_params.pickle', 'rb') as handle:
    params = pickle.load(handle)

with open(my_data_path + 'tree_samples.pickle', 'rb') as handle:
    samples = pickle.load(handle)

Probs = np.zeros((27,3))

for ind, params_name in enumerate(params.keys()):
    print(ind)
    if ind == 12:
    root = load_params(params[params_name])

    for i in range(1, 4):
        samples_name = params_name + '_sample_'+str(i)
        sample = samples[samples_name]
        load_sample(root, sample)


        Probs[ind, i-1] = s_algorithm(root)


print(Probs)
