import pickle
from ex_2_3 import Node, load_params, load_sample, print_tree
from ex_2_3_tree_helper import Tree
import numpy as np

def s(node, i, hash_s):

    key = node.name+'_'+str(i)
    if key in hash_s:
        return hash_s[key]

    if len(node.descendants) == 0:
        if i == node.sample:
            return 1
        else:
            return 0

    s_node = 1
    for n in node.descendants:
        prob = 0
        for j in range(0, len(n.cat[0])):
            s_value = s(n, j, hash_s)
            key = n.name + '_' + str(j)
            hash_s[key] = s_value
            prob += s_value*n.cat[i][j]
        s_node *= prob
    return s_node

def s_algorithm(root):
    hash_s = {}
    prob = 0
    for ind, p in enumerate(root.cat[0]):
        prob += s(root, ind, hash_s) * p
    return prob

my_data_path = ''

with open(my_data_path + 'tree_params.pickle', 'rb') as handle:
    params = pickle.load(handle)

with open(my_data_path + 'tree_samples.pickle', 'rb') as handle:
    samples = pickle.load(handle)

params_name = params.keys()[0]
params = params[params_name]
root = load_params(params)

samples_name = params_name + '_sample_1'
sample = samples[samples_name]
load_sample(root, sample)

print_tree(root)

prob = s_algorithm(root)

print(prob)
