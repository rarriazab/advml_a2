import pickle
import random
import itertools
import numpy as np
import math
from collections import defaultdict
from matplotlib.pylab import plt

def load_obj(name):
    with open('./' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def build_A():
    A = np.zeros((2, 2))
    A[0,0] = 0.25
    A[1,1] = 0.25
    A[1,0] = 0.75
    A[0,1] = 0.75
    return A

def chi(sequence, A, sigma, mu):
    alpha = forward(sequence, A, sigma, mu)
    beta = backwards(sequence, A, sigma, mu)
    K = len(A)
    E = []
    for n in np.arange(0, len(sequence)):
        e = np.zeros((K, K))
        for i in np.arange(0, K):
            for j in np.arange(0, K):
                p = eval_gauss(sequence[n], sigma, mu[n,j])
                e[i,j] = A[i, j] * p * alpha[n-1][i] * beta[n][j]
        E += [e]
    return E

def eval_gauss(x, sigma, mu):
    std = float(sigma) ** 2
    aux1 = 1/((2 * math.pi * std) ** 0.5)
    aux2 = math.exp(-(float(x) - float(mu)) ** 2 / (2 * std))
    return aux1*aux2

def prior_mu(seq_list, M):
    np.random.seed(101)
    mu = np.random.normal(15, 5, (M,2))
    mu_prior = {}
    for key in seq_list.keys():
        mu_prior[key] = mu
    return mu_prior

def prior_pi(seq_list):
    np.random.seed(100)
    pi = {}
    for key in seq_list.keys():
        pi[key] = np.array([0.5, 0.5])
    return pi

def prior_sigma(seq_list, M):
    np.random.seed(99)
    sigma = {}
    for player in seq_list.keys():
        sigma[player] = np.random.normal(10, 3, (M,2))
    return sigma

def forward(sequence, A, sigma, mu, pi):
    alpha = np.zeros((len(sequence), len(A)))
    c = np.zeros(len(sequence))
    b = np.array([eval_gauss(sequence[0], s, u) for (u, s) in zip(mu[0, :], sigma[0, :])])
    for i in range(0, len(A)):
        alpha[0,i] = pi[i]*b[i]
        c[0] += alpha[0,i]
    c[0] = 1/c[0]
    for i in range(0, len(A)):
        alpha[0, i] *= c[0]

    for t in np.arange(1, len(sequence)):
        for i in range(0, len(A)):
            for j in range(0, len(A)):
                alpha[t,i] += alpha[t-1,j]*A[i,j]
            b = eval_gauss(sequence[t], sigma[t, i], mu[t, i])
            alpha[t,i] = alpha[t,i]*b
            c[t] += alpha[t,i]
        c[t] = 1/c[t]
        for i in range(0, len(A)):
            alpha[t, i] *= c[t]

    return alpha, c

def backwards(sequence, A, sigma, mu, c):
    beta = np.zeros((len(sequence), len(A)))
    for i in range(0, len(A)):
        beta[len(sequence)-1, i] = c[len(sequence)-1]
    for t in reversed(range(len(sequence)-1)):
        for i in range(0, len(A)):
            for j in range(0, len(A)):
                b = eval_gauss(sequence[t+1], sigma[t+1, j], mu[t+1, j])
                beta[t, i] += A[i,j]*b*beta[t+1, j]
            beta[t, i] *= c[t]
    return beta

def Compute_gamma(alpha, beta, sequence, A, sigma, mu):
    gamma = np.zeros((len(sequence), 2))
    for t in np.arange(0, len(sequence)-1):
        denom = 0
        for i in np.arange(0, len(A)):
            for j in np.arange(0, len(A)):
                b = eval_gauss(sequence[t+1], sigma[t+1, j], mu[t+1, j])
                denom += alpha[t, i] * A[i, j] * b * beta[t+1, j]
        for i in np.arange(0, len(A)):
            for j in np.arange(0, len(A)):
                b = eval_gauss(sequence[t + 1], sigma[t+1, j], mu[t+1, j])
                a = (alpha[t, i]*A[i,j]*b*beta[t+1, j])/denom
                gamma[t, i] += a

    denom = 0
    for i in np.arange(0, len(A)):
        denom += alpha[-1,i]
    for i in np.arange(0, len(A)):
        gamma[-1, i] = alpha[-1, i]/denom

    return gamma + 1e-6

def run_E(sequences, A, sigma, mu, pi):
    Gammas_pairs = {}
    likelihood = {}
    px = 0
    for players in sequences.keys():
        Gammas_pairs[players] = {}
        likelihood[players] = 0#np.zeros((M, len(A)))
        for r in sequences[players]:
            alpha, c = forward(sequences[players][r], A, sigma[players], mu[players], pi[players])
            beta = backwards(sequences[players][r], A, sigma[players], mu[players], c)
            Gammas_pairs[players][r] = Compute_gamma(alpha, beta, sequences[players][r],
                                                     A, sigma[players], mu[players])
            likelihood[players] += -np.sum(np.log(c))
        likelihood[players] = likelihood[players]/len(sequences[players])
        px += likelihood[players]
    px = px/len(sequences.keys())
    return Gammas_pairs, px

def run_M(A, pi, Gammas):
    for players in Gammas.keys():
        gammas = Gammas[players]
        gamma1 = np.zeros(len(A))
        for r in gammas.keys():
            gamma1 += gammas[r][0,:]
        gamma1 = gamma1/len(gammas.keys())
        pi[players] = gamma1/np.sum(gamma1)
    return pi

def build_list_seq(sequence_outputs):
    sequences = {}
    for k in np.arange(0, len(sequence_outputs.keys())):
        pp1 = sequence_outputs.keys()[k]
        sequences[pp1] = {}
        for k2 in np.arange(0, len(sequence_outputs[pp1].keys())):
            try:
                sequences[pp1][k2] = np.array([i[0] for i in sequence_outputs[pp1][k2+1]])
            except:
                print('ups! Un error en la base de datos?')
    return sequences

def computeUS(Gammas, seq_list):
    # mu
    new_mu = {}
    for players in seq_list.keys():
        num = np.zeros(Gammas[players][0].shape)
        den = np.zeros(Gammas[players][0].shape)
        for r in seq_list[players]:
            x = np.array([seq_list[players][r], seq_list[players][r]]).T
            num += (Gammas[players][r]*x)
            den += Gammas[players][r]
        new_mu[players] = np.divide(num, den)
    #sigma
    sigma = {}
    for players in seq_list.keys():
        sigma[players] = np.zeros(Gammas[players][0].shape)
        num = np.zeros(Gammas[players][0].shape)
        den = np.zeros(Gammas[players][0].shape)
        for r in seq_list[players]:
            g = Gammas[players][r]
            x = np.array([seq_list[players][r], seq_list[players][r]]).T
            xu = x - new_mu[players]
            num += (g*np.power(xu,2))
            den += g
        sigma[players] = np.sqrt(np.divide(num, den)) + 1e-6
    return new_mu, sigma

def solver_mu(mu, M, N):
    A = {} # one for cell
    B = {} # one for cell
    Npairs = len(mu.keys())

    for s in np.arange(0, 2*M):
        A[s] = np.zeros((Npairs, N))
        B[s] = np.zeros(Npairs)
    for i, players in enumerate(mu.keys()):
        if mu[players][0, 0] > mu[players][0, 1]:
            new_mu = mu[players][:, ::-1]
        else:
            new_mu = mu[players]
        mu_array = new_mu.ravel(order='F')
        for s in np.arange(0, len(mu_array)):
            A[s][i, players[0] - 1] = 1
            A[s][i, players[1] - 1] = 1
            B[s][i] = mu_array[s]

    U = np.zeros((2*M, N))
    for s in np.arange(0, 2 * M):
        aux = np.linalg.pinv(A[s].T.dot(A[s]))
        x = aux.dot(A[s].T.dot(B[s].reshape(-1,1)))
        U[s,:] = x.ravel()
    return U

def computeBeta(sigma):
    betas = np.zeros((M, 2))
    for player in sigma.keys():
        betas += sigma[player]
    betas = betas/len(sigma.keys())
    Mean_sigma = np.mean(np.mean(betas))
    beta = np.sqrt((Mean_sigma**2)/2)
    return beta

M = 30
N = 20
R = 10
beta = math.pi
sigma = 5

# mean for each subfield with respect to the player
mu_rand = np.array([np.random.normal(10, sigma, (M,N)), np.random.normal(23, sigma, (M,N))])
# database
sequence_outputs = load_obj("sequence_output")
mu_targetpkl = load_obj("mu_target")
mu_target = np.concatenate((mu_targetpkl[0], mu_targetpkl[1]), axis=0)
seq_list = build_list_seq(sequence_outputs)
#params
A = build_A()
pi_prior = prior_pi(seq_list)
mu_prior = prior_mu(seq_list, M)
prior_sigma = prior_sigma(seq_list, M)

#algorithm
sigma = prior_sigma
pi = pi_prior
mu = mu_prior
px_old = 0
px_new = np.inf
iter = 0
like = []
while(abs(px_new-px_old)>0.01):
    px_old = px_new
    Gammas, px = run_E(seq_list, A, sigma, mu, pi)
    pi = run_M(A, pi, Gammas)
    mu, sigma = computeUS(Gammas, seq_list)
    px_new = np.sum(np.sum(px))
    b = computeBeta(sigma)

    U = solver_mu(mu, M, N)
    error = np.mean(abs(U - mu_target) / mu_target)
    print('Iteration {}'.format(iter))
    print('beta: {}'.format(b))
    print('pi: {}'.format(pi))
    print('likelihood: {}'.format(px))
    print('Diff: {}'.format(abs(px_new-px_old)))
    print('Error1: {}'.format(error))
    print('\n')
    like += [px]
    iter += 1
plt.plot(np.arange(0, len(like)), like)
plt.xlabel('Iteration', fontsize = 18)
plt.ylabel('Log-Likelihood', fontsize = 18)
plt.savefig('2_7_likelihood.eps')
plt.show()
U = solver_mu(mu, M, N)
error = np.mean(abs(U-mu_target)/mu_target)
print('Final Absolute Error mus {}'.format(error))

